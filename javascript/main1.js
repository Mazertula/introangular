var myApp = angular.module("myApp", [
    "ngRoute",
    'youtube-embed'
    //"ui.router"
    //,"ui.bootstrap" 
    //,"oc.lazyLoad"
]); 


//Define Routing for app
myApp.config(['$routeProvider',
  function($routeProvider) {
      $routeProvider.
      when('/list', {
        templateUrl: 'list.html',
        controller: 'ListCtrl'
    }).
      when('/show/:videoId', {
        templateUrl: 'show.html',
        controller: 'ShowCtrl'
      }).
      otherwise({
        redirectTo: '/list'
      });
}]);

//Controllers
//List
myApp.controller('ListCtrl', ['$scope', '$http', '$rootScope',
    function ($scope, $http, $rootScope) {
        
        //init search object
        $scope.search = {
            term: '',
            rows: 20
        };
        
        //call the search api
        $scope.launchSearch = function () {
            
            //call the api
            $http.post('/search', $scope.search).then(function (res) {
                
                //log the response data in the console
                console.log(res.data);
                
                //put the result in resVideo local object
                $scope.result = res.data;
            });
        };

    }]);

//show
myApp.controller('ShowCtrl', ['$scope', '$http', '$rootScope', '$routeParams','$sce',
    function ($scope, $http, $rootScope, $routeParams, $sce) {  
        // $sce.trustAsResourceUrl ('https://www.youtube.com/embed/' + $routeParams.videoId);
        //$scope.videoId = $routeParams.videoId;
        $scope.urlVideoId =$sce.trustAsResourceUrl ('https://www.youtube.com/embed/' + $routeParams.videoId);
}]);