var todoApp = angular.module('todoApp', ["ngRoute"]);


//Define Routing for app
todoApp.config(['$routeProvider',
  function($routeProvider) {
      $routeProvider.
      when('/todo', {
        templateUrl: 'todoView5.html',
        controller: 'TodoCtrl'
      }).
    when('/done', {
        templateUrl: 'done.html',
        controller: 'DoneCtrl'
    }).
      when('/notdone', {
        templateUrl: 'done.html',
        controller: 'NotDoneCtrl'
      }).
      when('/compare', {
        templateUrl: 'compare.html',
        controller: 'CompareCtrl'
      }).
      when('/youtube', {
        templateUrl: 'youtube.html',
        controller: 'YoutubeCtrl'
      }).
      otherwise({
        redirectTo: '/todo'
      });
}]);

todoApp.controller('NavCtrl', ['$scope', '$location',
    function($scope, $location){
    $scope.isActive = function (viewLocation) { 
        //console.log($location.path());
        return viewLocation === $location.path();
    };
}]);

todoApp.controller('YoutubeCtrl', ['$scope', '$http', function ($scope, $http) {
    
    // $scope.search = function(){
    //     console.log("lol");        
    // };
    
    //init search object
    $scope.searchObject = {
        term: '',
        rows: 20
    };
        
    //call the search api
    $scope.search = function () {
        
        //call the api
        $http.post('/search', $scope.searchObject).then(function (res) {
            
            //log the response data in the console
            console.log(res.data);
            
            //put the result in resVideo local object
            $scope.result = res.data;
        });
    };
    
}]);

todoApp.controller('CompareCtrl', ['$scope', '$http', function ($scope, $http) {
    //console.log("Hello");
    $scope.result = function () {
        
        if (angular.isUndefined($scope.valA) || angular.isUndefined($scope.valB))
        { 
            return "";
        }else if ($scope.valA > $scope.valB) {
            return "A plus grand que B";
        } else if ($scope.valA < $scope.valB) {
            return "B plus grand que A";          
        } else {
            return "A = B";
        }
    }
}]);

todoApp.controller('DoneCtrl', ['$scope', '$http', 'TodoService', function ($scope, $http, TodoService) {
    
    $scope.taskSet = TodoService.getTaskDoneSet(); 
   
}]);


todoApp.controller('NotDoneCtrl', ['$scope', '$http', 'TodoService', function ($scope, $http, TodoService) {
    
    $scope.taskSet = TodoService.getTaskNotDoneSet(); 
   
}]);

todoApp.controller('TodoCtrl', ['$scope', '$http','TodoService', function ($scope, $http, TodoService) {
    
    //init array of task
    $scope.taskSet = TodoService.getTaskSet(); 
    
    //$scope.nbItem =  TodoService.getTaskSet().length;
    
    $scope.$watchCollection('taskSet', function(newNames, oldNames) {
        $scope.nbItem = newNames.length;
    });
    
    //Add new task in list
    $scope.addTask = function () {
        if ($scope.taskName) {  
            TodoService.addTask($scope.taskName);
            $scope.taskName = '';
        }
    };
    
    //delete selected task in list
    $scope.delTask = function(task){
       TodoService.delTask(task); 
    } 
    
}]);

todoApp.factory('TodoService', ['$filter', function ($filter) {

    //var todoService = {};
    var taskSet = [];
    var getTaskSet = localStorage.getItem("taskSet");
    taskSet = JSON.parse(getTaskSet);

    var serv =  {};
    
    function saveTask() {
        localStorage.setItem("taskSet", angular.toJson(taskSet));
    };
    
    serv.getNumber = function(){
        return taskSet.length;
    }
    
    serv.getTaskSet = function () {
        return taskSet;
    };
        
    serv.getTaskDoneSet = function () {
        return taskSet.filter(function (task) { return task.done; })
    };
        
    serv.getTaskNotDoneSet = function () {
        return taskSet.filter(function (task) { return !task.done; })
    };
    
    serv.addTask =  function (taskName) {
        
        //add task to array
        taskSet.push({
            taskName: taskName,
            done: false
        });
        
        //save array
        saveTask();
    };
    
    serv.delTask = function (task) {
        
        //find index
        var index = taskSet.indexOf(task);
        
        //delete entry
        taskSet.splice(index, 1);
        
        //save array
        saveTask();
    };
        
    
    return serv;

}]);
