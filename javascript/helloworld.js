var helloApp = angular.module('helloApp', []);

helloApp.controller('helloCtrl', ['$scope', function ($scope) {
    $scope.name = 'World';
}]);