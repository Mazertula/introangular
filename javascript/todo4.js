var todoApp = angular.module('todoApp', []);

todoApp.controller('TodoCtrl', ['$scope', '$http', function ($scope, $http) {
    
    //init array of task
    $scope.taskSet = [];
    
    //load data from
    var getTaskSet = localStorage.getItem("taskSet"); 

    if (getTaskSet) {
        $scope.taskSet = JSON.parse(getTaskSet); 
    }
    
    //Add new task in list
    $scope.addTask = function () {
        
        if ($scope.taskName !== '') {
         
            //add task to array
            $scope.taskSet.push({
                taskName: $scope.taskName,
                done: false
            });
            
            //clear taskName value
            $scope.taskName = '';
            
            //save array
            $scope.saveTaskSet();
        }
    };
    
    //delete selected task in list
    $scope.delTask = function (task) {
        
        //find index
        var index = $scope.taskSet.indexOf(task);
        
        //delete entry
        $scope.taskSet.splice(index, 1);
        
        //save array
        $scope.saveTaskSet();
    };
    
    //save
    $scope.saveTaskSet = function () {
        
        localStorage.setItem("taskSet", JSON.stringify($scope.taskSet));
        
        
        $http.post('/test', $scope.taskSet).then(function (res) {
            console.log(res.data);
        });
    };
    
}]);

